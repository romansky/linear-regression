#include "matrix.h"
#include "helper.h"

using namespace std;

int main(int arg, char *argv[])
{
    matrix A(10,10);
    A.fillMatrix(A);
    matrix Ai = A.inverse(A);
    
    return 0;
}
