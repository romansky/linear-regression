matrix::matrix(int r, int c)
{
    row = r;
    col = c;
    // Allocates memory 
    arr = new int *[r];

    for(int i=0; i<r; i++)
    {
        arr[i] = new int[c];
    }
}

matrix::matrix(const matrix &n)
{
    row = n.row;
    col = n.col;

    arr = new int *[row];

    for(int i=0; i < row; i++)
    {
        arr[i] = new int[col];
    }

    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            arr[i][j] = n.arr[i][j];
        }
    }
}

matrix::~matrix()
{
    for(int i=0; i < row; i++)
    {
        delete [] arr[i];
    }

    delete [] arr;
}

matrix matrix::operator = (const matrix &n)
{
    row = n.row;
    col = n.col;

    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            n.arr[i][j] = arr[i][j];
        }
    }
    return n;
}

matrix matrix::operator + (const matrix &n)
{
    matrix sum(row,col);

    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            sum.arr[i][j] = arr[i][j] + n.arr[i][j];
        }
    }
    return sum;
}

matrix matrix::operator * (const matrix &n)
{
    matrix prod(row,col);
    int tmp = 0;

    for(int i=0; i < row; i++)
    {
        for(int j=0; j < n.col; j++)
        {
            tmp = 0;
            for(int k=0; k < col; k++)
            {
                tmp += (arr[i][k] * n.arr[k][j]);
                prod.arr[i][j] = tmp;
            }
        }
    }
    return prod;
}

matrix matrix::operator * (const int n)
{
    matrix prod(row,col);

    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            prod.arr[i][j] = arr[i][j] * n;
        }
    }
    return prod;
}

matrix matrix::identity(const matrix n)
{
    matrix id(row, col);
    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            if(i==0 && j==0)
            {
                id.arr[i][j] = 1;
            } else {
                id.arr[i][j] = 0;
                id.arr[i][j] = 0;
            }
        }
    }
    return id;
}

matrix matrix::transpose(const matrix &n)
{
    matrix trans(col,row);
    for(int i=0; i < row; i++)
    {
        for(int j=i; j < col; j++)
        {
            swap(trans.arr[i][j],trans.arr[j][i]);
        }
    }
    return trans;
}

matrix matrix::pad(const matrix &n)
{
    matrix p(row,col);

    int size = 2;

    while((size < row || size < col) && (size > 0))
    {
        size = size * 2;
    }

    int **oldArr = arr;
    int oldRow = row;
    int oldCol = col;
    row = size;
    col = size;

    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++){
            if(i >= oldRow || j >= oldCol)
            {
                arr[i][j] = 1;
            } else {
                arr[i][j] = 0;
            }
        }
    }

    delete oldArr;

    return p;
}
matrix matrix::inverse(const matrix &n)
{
    int subRow = row / 2;
    int subCol = col / 2;

    matrix B(subRow,subCol);
    matrix C(subRow,subCol);
    matrix Ct = transpose(C);
    matrix D(subRow,subCol);

    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            if((i < subRow) && (j < subCol)) {
                B.arr[i][j] = n.arr[i][j];
            } else if ((i < subRow) && (j >= subCol)) {
                Ct.arr[i][j - subCol] = n.arr[i][j];
            } else if ((i >= subRow) && (j < subCol)) {
                C.arr[i - subRow][j] = n.arr[i][j];
            } else if ((i >= subRow) && ( j >= subCol)) {
                D.arr[i - subRow][j - subCol] = n.arr[i][j];
            }
        }
    }

    matrix Bi = inverse(B);

    matrix W = C * Bi;

    matrix X = W * Ct;

    matrix S = D + (X * (-1));

    matrix V = inverse(S);

    matrix Y = V * W;

    matrix Yt = transpose(Y);

    matrix L = Yt * -1;

    matrix U = Y * -1;

    matrix Z = transpose(W) * Y;

    matrix R = Bi + Z;

    // I got lost on how to assemble the matrix after computing the inverse(s)
    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            if((i < subRow) && (j < subCol)) {
                n.arr[i][j] = R.arr[i][j];
            } else if ((i < subRow) && (j >= subCol)) {
                n.arr[i][j] = L.arr[i][j - subCol];
            } else if ((i >= subRow) && (j < subCol)) {
                n.arr[i][j] = U.arr[i - subRow][j];
            } else if ((i >= subRow) && ( j >= subCol)) {
                n.arr[i][j] = V.arr[i - subRow][j - subCol];
            }
        }
    }

    return n;

}

void matrix::fillMatrix(matrix &n)
{
    srand(time(0));
    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            arr[i][j] = (rand()%10) + 1;
        }
    }
}

void matrix::print(matrix n)
{ 
    for(int i=0; i < row; i++)
    {
        for(int j=0; j < col; j++)
        {
            cout << n.arr[i][j] << " ";
        }
        cout << endl;
    }
}
