#ifndef MATRIX_H
#define MATRIX_H

#include "helper.h"
#include <ctime>
#include <random>
#include <iostream>

using namespace std;

class matrix
{
    private:
        int row;
        int col;
        int **arr;

    public:
        matrix(int r, int c);
        matrix(const matrix &n);
        ~matrix();
        matrix operator = (const matrix &n);
        matrix operator + (const matrix &n);
        matrix operator - (const matrix &n);
        matrix operator * (const matrix &n);
        matrix operator * (const int n);

        matrix identity(const matrix n); // Finds the identity matrix
        matrix transpose(const matrix &n); // Finds the transpose of a given matrix
        matrix inverse(const matrix &n); // Finds the inverse of a given matrix
        matrix pad(const matrix &n);
        void fillMatrix(matrix &n);
        void fill(); // Fills the matrix with vectors from the .dat file
        void print(matrix n);
};


#include "matrix.cpp"
#endif
