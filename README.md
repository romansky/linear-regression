# Linear Regression with Ordinary Least Square

### Installing

Navigate to the linear-regression directory and type:

```
$ make
```

## Running the tests

I attempted to run multiple tests to try to solve the matrix arithmetic

## Authors

* **Billy Romansky** - *Initial work* - [Salisbury University]

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
